# coding: utf-8

import numpy as np


# Scikit kernels
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as Ck, WhiteKernel
from sklearn.gaussian_process.kernels import ExpSineSquared, DotProduct
from sklearn.gaussian_process.kernels import Matern, RationalQuadratic

# Model
# from mult_model_debug import M2GP
from mult_model import M2GP
from mult_model_indep import M2GP as M2GP_indep
from indep_model import MIMGP


# Parameters
n_sig_per_class = 50
n_classes = 4
nFeatures = 3
kernel = Ck(1.0, constant_value_bounds=(0.0, 10.0)) \
        * RBF(length_scale=24, length_scale_bounds=(0.0, 300.0)) \
        + WhiteKernel(0.01)
# kernel = Ck(1.0, constant_value_bounds=(0.0, 10.0)) \
#         * RBF(length_scale=0.5, length_scale_bounds=(0.0, 10.0)) \
#         + WhiteKernel(0.01)
tau_ = 365


def generate_mean(c = 0, p = 3, tau = 365):
    t_ = np.arange(0, tau)
    
    means = []
    for b in range(p):
        if c == 0:
            means.append(b * np.ones_like(t_))
        elif c == 1:
            means.append(np.sin(t_ + b))
        elif c == 2:
            means.append((1-b)*np.cos(t_) + (b)*np.sin(t_))
        elif c == 3:
            means.append(np.power(t_/tau, 2) + b * (t_/tau))
        else:
            means.append(b * np.ones_like(t_) + c)

    A_c = (c*0.5 + 1) * np.eye(p)
    if c == 1:
        A_c[0, p-1] = 1; A_c[p-1, p-1] = 3
    elif c == 2:
        A_c[0, :] = np.arange(1, p+1)
    elif c == 3:
        A_c[:, p-1] = 0.5
    else:
        A_c[0, 0] = 1

    
    return A_c, np.array(means)

def generate_GPs(c, A_c, means_c, K, n_sig = 100, tau = 365):
    # Init
    GPs = [[] for _ in range(n_sig)]
    Cs = []
    # Number of times
    n_times = np.random.randint(5, tau/2, size = n_sig)
    
    for i in range(n_sig):
        # Random times
        t_i = np.sort(np.unique(np.random.randint(1, tau, size = n_times[i])))
        K_i = K(t_i.reshape([-1, 1]))
        null_mean_i = np.zeros_like(t_i)
        # Fill each samples
        GPs[i].append(t_i)
        for b in range(A_c.shape[0]):
            GPs[i].append(np.random.multivariate_normal(
                null_mean_i, K_i))

        # Transform GPs
        GPs[i][1:] = np.dot(A_c, GPs[i][1:])
        GPs[i][1:] += means_c[:, t_i]
        # Fill classes
        Cs.append(c)

    return np.array(GPs, dtype = object), np.array(Cs)


def get_time_series(seed = 0, n_c = 3, p = 3, tau = 365, n_sigc = 100):
    
    np.random.seed(seed)
    means = []

    for c in range(n_c):
        # Extract mean and covariances matrices
        A_c_, means_c = generate_mean(c = c, p = p, tau = tau)
        # Generate random multivariate normal signals
        Gps_, Cs_ = generate_GPs(c = c, A_c = A_c_, means_c = means_c,
                                 K = kernel, n_sig = n_sigc, tau = tau)
        # Concatenate for each class
        means.append(means_c)
        if c == 0:
            Gps, Cs = Gps_, Cs_
            A_c = A_c_[np.newaxis, ...]
        else:
            Gps = np.concatenate([Gps, Gps_], axis = 0)
            Cs = np.concatenate([Cs, Cs_], axis = 0)
            A_c = np.concatenate([A_c, A_c_[np.newaxis, ...]], axis = 0)

    return Gps, Cs, A_c, means


def main(n_c = 3, p = 3, tau = 365, n_sigc = 100):
    
    np.random.seed(0)

    for c in range(n_c):
        # Extract mean and covariances matrices
        A_c_, means_c = generate_mean(c = c, p = p, tau = tau)
        # Generate random multivariate normal signals
        Gps_, Cs_ = generate_GPs(c = c, A_c = A_c_, means_c = means_c,
                                 K = kernel, n_sig = n_sigc, tau = tau)
        # Concatenate for each class
        if c == 0:
            Gps, Cs = Gps_, Cs_
            A_c = A_c_[np.newaxis, ...]
        else:
            Gps = np.concatenate([Gps, Gps_], axis = 0)
            Cs = np.concatenate([Cs, Cs_], axis = 0)
            A_c = np.concatenate([A_c, A_c_[np.newaxis, ...]], axis = 0)

    CLASSES = np.unique(Cs)
    # Learn model
    K = Ck(1.0, constant_value_bounds=(1e-05, 1e5)) \
        * Matern(1., length_scale_bounds=(1e-05, 1e5), nu = 0.5) \
        + WhiteKernel(noise_level=0.01,
                      noise_level_bounds=(1e-10, 1e+1))
    K = Ck(25.0, constant_value_bounds=(1e-5, 100.0)) \
        * RBF(length_scale=0.5, length_scale_bounds=(1e-5, 5.0)) \
        + RBF(length_scale=45.0, length_scale_bounds=(5.0, 100)) \
        + WhiteKernel(0.0001)
    basis_ = "fourier"
    n_basis_ = 21
    model = M2GP(kernel = K, epsilon = 1e-16, base = basis_, n_basis = n_basis_, tau = tau_)
    modeli = M2GP_indep(kernel = K, epsilon = 1e-16, base = basis_, n_basis = n_basis_, tau = tau_)
    modelid = MIMGP(kernel = K, epsilon = 1e-16, base = basis_, n_basis_ = n_basis_, tau = tau_)

    model.fit(Gps, Cs)
    modeli.fit(Gps, Cs)
    modelid.fit(Gps, Cs)

    # # If debug, save parameters
    # iters_save_table = model.iters
    # iters_thetas = model.th_iters

    # lmls = [[iters_save_table[c][0][it][0] \
    #          for it in range(len(iters_save_table[c][0]))] \
    #         for c in range(len(iters_save_table))]
    # AA_T = [[iters_save_table[c][0][it][1] \
    #          for it in range(len(iters_save_table[c][0]))] \
    #         for c in range(len(iters_save_table))]
    # alph = [[iters_save_table[c][0][it][2] \
    #          for it in range(len(iters_save_table[c][0]))] \
    #         for c in range(len(iters_save_table))]

    # np.savez('simus/params_debug/cvgce_theta.npz',
    #          thetas = np.array(iters_thetas, dtype = object),
    #          alphas = np.array(alph, dtype = object),
    #          AA_Ts  = np.array(AA_T, dtype = object),
    #          lmls   = np.array(lmls, dtype = object))

    
    # Print parameters
    for key in model.kernels:
        print("\nKernel {}: {}".format(CLASSES[int(key)], model.kernels[key]))
        print("Kernel_indep {}: {}".format(CLASSES[int(key)], modeli.kernels[key]))
    for c in range(CLASSES.shape[0]):
        print(c)
        AA_cT_true = np.dot(A_c[c], A_c[c].T)
        print(model.AA_T_[c])
        print(AA_cT_true)

    print(model.score(Gps, Cs))
    print(modeli.score(Gps, Cs))
    print(modelid.score(Gps, Cs))
    
    return 0

if __name__ == "__main__":
    main(n_c = n_classes, p = nFeatures, tau = tau_, n_sigc = n_sig_per_class)
