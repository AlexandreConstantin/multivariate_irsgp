# coding: utf8
import warnings
import imp
from operator import itemgetter
import numpy as np

from scipy.linalg import cholesky, cho_factor, cho_solve, solve_triangular, solve
from scipy.linalg import LinAlgWarning, LinAlgError
from scipy.optimize import fmin_l_bfgs_b
#from scipy.special import softmax

from joblib import Parallel, delayed

from sklearn.base import BaseEstimator, clone
from sklearn.utils import check_random_state
from sklearn.utils.validation import check_array

from sklearn import preprocessing

# Kernel depedency
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as C, WhiteKernel
from sklearn.exceptions import ConvergenceWarning
# Score metrics
from sklearn.metrics import accuracy_score


# TODO: tau needs to be estimated from the data - Ajouté dans init_design_matrix
#       mais à voir, dans notre cas c'est le max des instants multiplié par 2 (demie période)
# TODO: parallization of restarts
# Concernant mon erreur "DepreciationWarning" elle disparait si je mets "import imp" ci-dessus..


def constrained_optimization(obj_func, initial_theta, bounds,
                             X, t, c, b, optimizer):
    if optimizer == "fmin_l_bfgs_b":
        theta_opt, func_min, convergence_dict = \
                            fmin_l_bfgs_b(obj_func, initial_theta,
                                          args=(True, X, t, c, b),
                                          bounds=bounds,
                                          maxls=20)
        if convergence_dict["warnflag"] != 0:
            warnings.warn("fmin_l_bfgs_b terminated abnormally with the "
                          " state: %s" % convergence_dict,
                          ConvergenceWarning)
    elif callable(optimizer):
        theta_opt, func_min = optimizer(obj_func, initial_theta,
                                        args=(True, X, t, c, b),
                                        bounds=bounds)
    else:
        raise ValueError("Unknown optimizer %s." % optimizer)

    return theta_opt, func_min


def fourier_basis_functions(t, J, tau):
    """
    Input : t,   float, instant of observation
            J,     int, number of basis function
            tau, float, observed period
    Output: array[float], fourier basis function
    """
    n_bases = J.size
    n_functions = int((J.size-1)/2)
    b = np.empty((n_bases,))
    b[0] = 1
    for j in range(1, 1+n_functions):
        b[j] = np.cos(2*np.pi*j*t/tau)
        b[n_functions+j] = np.sin(2*np.pi*j*t/tau)

    return b


def exp_basis_functions(t, J, tau):
    n_bases = J.size
    b = np.empty((n_bases,))
    space_between_exp = tau/(n_bases-1)
    gamma = 8*space_between_exp
    b[0] = np.exp(-0.5*(t)**2/gamma)
    b[-1] = np.exp(-0.5*(tau-t)**2/gamma)
    for b_, base_center in enumerate(np.linspace(space_between_exp,
                                                 tau-space_between_exp,
                                                 n_bases-2)):
        b[b_+1] = np.exp(-0.5*(t-base_center)**2/gamma)
    return b/b.sum()


class MIMGP(BaseEstimator):
    """Irregularly Sampled Classification based on Gaussian Processes

    Parameters
    ----------
    kernel: kernel object, default is None
        The kernel specifying the covariance function of the Gaussian Process.

    tau:  int, optional (default: None)
        Highest value possible for time (used in the design matrix).
        Default (None) will be inferred from the data.

    n_basis_: int, optional (default: 10)
        size of alpha used for mean projection of the Gaussian Process,

    base: callable, optional (default: None)
        function used to build design matrix, 
        if None a Fourier Basis is used.

    optimizer: string or callable, optional (default: "fmin_l_bfgs_b")
        Can either be one of the internally supported optimizers for optimizing
        the kernel's parameters, specified by a string, or an externally
        defined optimizer passed as a callable. If a callable is passed, it
        must have the signature::
            def optimizer(obj_func, initial_theta, bounds):
                # * 'obj_func' is the objective function to be minimized, which
                #   takes the hyperparameters theta as parameter and an
                #   optional flag eval_gradient, which determines if the
                #   gradient is returned additionally to the function value
                # * 'initial_theta': the initial value for theta, which can be
                #   used by local optimizers
                # * 'bounds': the bounds on the values of theta
                ....
                # Returned are the best found hyperparameters theta and
                # the corresponding value of the target function.
                return theta_opt, func_min
        Per default, the 'fmin_l_bfgs_b' algorithm from scipy.optimize
        is used. If None is passed, the kernel's parameters are kept fixed.
        Available internal optimizers are::
            'fmin_l_bfgs_b'

    opt_gradient: string, 'complete' or 'alternate' (default: 'alternate')
        'complete' - Will compute the gradient descent for theta at once,
                     then compute the best mean's projection coefficients.
        'alternate'- Will compute the gradient descent by alternating theta
                     and mean's projection coefficients.

    n_restarts_optimizer: int, optional (default: 0)
        The number of restarts of the optimizer for finding the kernel's
        parameters which maximize the log-marginal likelihood. The first run
        of the optimizer is performed from the kernel's initial parameters,
        the remaining ones (if any) from thetas sampled log-uniform randomly
        from the space of allowed theta-values. If greater than 0, all bounds
        must be finite. Note that n_restarts_optimizer == 0 implies that one
        run is performed.

    epsilon: float or array-like, optional (default: 1e-10)
        Value added to the diagonal of the kernel matrix during fitting.
        Larger values correspond to increased noise level in the observations.
        This can also prevent a potential numerical issue during fitting, by
        ensuring that the calculated values form a positive definite matrix.
        If an array is passed, it must have the same number of entries as the
        data used for fitting and is used as datapoint-dependent noise level.
        Note that this is equivalent to adding a WhiteKernel with c=alpha.
        Allowing to specify the noise level directly as a parameter is mainly
        for convenience and for consistency with Ridge.

    copy_X_train: bool, optional (default: True), Probably to be deleted.

    random_state: int, RandomState instance or None, optional (default: None)
        The generator used to initialize the centers. If int, random_state is
        the seed used by the random number generator; If RandomState instance,
        random_state is the random number generator; If None, the random number
        generator is the RandomState instance used by `np.random`.
    rc
    Attributes
    ----------
    
    """
    def __init__(self, kernel=None, epsilon=1e-12,
                 base="fourier", n_basis_=10, tau=None,
                 optimizer="fmin_l_bfgs_b", opt_gradient = 'alternate',
                 n_restarts_optimizer=0, copy_X_train=True,
                 random_state=None):
        # Time range (0, tau) for the time-series
        self.tau = tau

        # Kernel definition - sklearn function
        self.kernel = kernel
        self.epsilon = epsilon

        # Mean, design matrix definition
        self.alpha_ = None
        self.B = None
        self.dict_position = {}

        if callable(base):
            self.n_basis_ = n_basis_
            self.func_base = base
        elif base == "polynomial":
            self.n_basis_ = n_basis_
            self.func_base = lambda t_, j, tau_: np.power(t_ / tau_, j)
        elif base == "fourier":
            self.n_basis_ = n_basis_ * 2 + 1
            self.func_base = fourier_basis_functions
        else:
            self.n_basis_ = n_basis_+2
            self.func_base = exp_basis_functions

        self.compute_alpha = True

        # Optimization parameters
        self.optimizer = optimizer
        self.opt_gradient = opt_gradient
        self.n_restarts_optimizer = n_restarts_optimizer
        self.log_marginal_likelihood_value_ = 0

        # Data settings
        self.copy_X_train = copy_X_train
        self.random_state = random_state

    def fit(self, X, y, compute_likelihood=False):
        """
        Input : X, array-like, shape = (n_samples,
                                        1 + n_features, list of T_y instants)
                   the first feature is the observation instants of X
                y, array: class labels (transformed using preprocessing);
                   structured as [n samples]
        Output: self
        """
        # Init random state
        self._rng = check_random_state(self.random_state)

        # Prepare data: split sample time & features
        X_train_ = np.copy(X[:, 1:])
        self.n = X_train_.shape[0]
        self.n_bands_ = X_train_.shape[1]
        t_ = np.copy(X[:, 0])

        # Init design matrix
        self.init_design_matrix(t_)

        # Preprocess classes
        le = preprocessing.LabelEncoder()
        y_train_ = le.fit_transform(y)
        self.classes_ = le.classes_
        self.n_classes_ = le.classes_.size

        # Compute weights
        self.index_C = [np.where(y_train_ == c)[0]
                        for c in range(self.n_classes_)]

        self.weights_ = np.array([float(ind_c.size)/self.n
                                  for ind_c in self.index_C])

        # Initialization alpha
        self.alpha_ = np.empty([self.n_classes_, self.n_bands_, self.n_basis_])

        # Kernels definition
        kernel_ = C(1.0, constant_value_bounds=(1e-5, 1e5)) \
            * RBF(1.0, length_scale_bounds=(1e-05, 1e5)) \
            + WhiteKernel(noise_level=0.1,
                          noise_level_bounds=(1e-10, 1e+1)) \
            if self.kernel is None else self.kernel

        self.kernels = {"{0}{1}".format(c, b): clone(kernel_)
                        for c in range(self.n_classes_)
                        for b in range(self.n_bands_)}

        if self.optimizer:
            # Objectif function for fixed c and b
            def obj_func(theta, eval_gradient=True, *args):
                if eval_gradient:
                    lml, grad = self.log_marginal_likelihood(theta,
                                                             eval_gradient,
                                                             args[0], args[1],
                                                             args[2], args[3])
                    return -lml, -grad
                else:
                    return -self.log_marginal_likelihood(theta, eval_gradient,
                                                         args[0], args[1],
                                                         args[2], args[3])

            # Optimization on c and b at once - Not working like this
            thetaCB_ = Parallel(n_jobs=-1)(
                delayed(constrained_optimization)(
                    obj_func,
                    self.kernels["{0}{1}".format(c, b)].theta,
                    self.kernels["{0}{1}".format(c, b)].bounds,
                    X_train_[self.index_C[c], :],
                    t_[self.index_C[c]],
                    c, b, self.optimizer)
                for c in range(self.n_classes_)
                for b in range(self.n_bands_))

            thetaCB = {"theta{0}{1}".format(c, b):
                       [thetaCB_.pop(0)]
                       for c in range(self.n_classes_)
                       for b in range(self.n_bands_)}

            # Serial implementation

            # thetaCB = {"theta{0}{1}".format(c, b):
            #            [(self._constrained_optimization(
            #                obj_func,
            #                self.kernels["{0}{1}".format(c, b)].theta,
            #                self.kernels["{0}{1}".format(c, b)].bounds,
            #                X_train_[self.index_C[c], :],
            #                t_[self.index_C[c]],
            #                c, b))]
            #            for c in range(self.n_classes_)
            #            for b in range(self.n_bands_)}

            if (self.n_restarts_optimizer > 0):
                # Additional runs are performed from log-uniform chosen initial
                # theta bounds
                kernel_bounds = [self.kernels["{0}{1}".format(c, b)].bounds
                                 for c in range(self.n_classes_)
                                 for b in range(self.n_bands_)]

                if not np.isfinite(kernel_bounds).all():
                    raise ValueError("Multiple optimizer restarts"
                                     "(n_restarts_optimizer>0)"
                                     "requires that all bounds are finite.")

                for iteration in range(self.n_restarts_optimizer):
                    for c in range(self.n_classes_):
                        for b in range(self.n_bands_):
                            theta_initial = \
                                self._rng.uniform(
                                    self.kernels["{0}{1}".format(c, b)].bounds[:, 0],
                                    self.kernels["{0}{1}".format(c, b)].bounds[:, 1])

                            thetaCB["theta{0}{1}".format(c, b)].append(
                                self._constrained_optimization(
                                    obj_func,
                                    theta_initial,
                                    self.kernels["{0}{1}".format(c, b)].bounds,
                                    X_train_[self.index_C[c], :],
                                    t_[self.index_C[c]],
                                    c, b))
            
            # Select result from run with minimal (negative) log-marginal
            # likelihood
            self.log_marginal_likelihood_value_ = 0
            for c in range(self.n_classes_):
                for b in range(self.n_bands_):
                    lml_values_cb = list(map(itemgetter(1),
                                             thetaCB["theta{0}{1}".format(c, b)]))
                    i_min = np.argmin(lml_values_cb)
                    # Store theta
                    self.kernels["{0}{1}".format(c, b)].theta = \
                        thetaCB["theta{0}{1}".format(c, b)][i_min][0]
                    # Store lml
                    self.log_marginal_likelihood_value_ += \
                        thetaCB["theta{0}{1}".format(c, b)][i_min][1]

        # Compute alpha for fixed or optimized theta
        for c in range(self.n_classes_):
            left_part = np.zeros((self.n_bands_,
                                  self.n_basis_, self.n_basis_))
            right_part = np.zeros((self.n_bands_,
                                   self.n_basis_))

            # Compute summation terms for alpha
            for ind_ in self.index_C[c]:
                for b in range(self.n_bands_):
                    # Compute design matrix
                    Bi = self.design_matrix(t_[ind_], in_memory=True)

                    # Compute kernel matrix
                    K = self.kernels["{0}{1}".format(c, b)](
                        t_[ind_].reshape([t_[ind_].shape[0], 1]))
                    K[np.diag_indices_from(K)] += self.epsilon

                    # Solve linear problem
                    L = cho_factor(K, lower=True)
                    M = cho_solve(L, Bi).T

                    # Update left and right parts
                    left_part[b, :, :] += np.dot(M, Bi)
                    right_part[b, :] += np.dot(M, X_train_[ind_, b])

            # Get optimal alpha for class c and band b
            # warning from scipy.linalg is not printed
            warnings.filterwarnings("error", category = LinAlgWarning)
            for b in range(self.n_bands_):
                try:
                    self.alpha_[c, b, :] = solve(left_part[b, :, :],
                                                 right_part[b, :])
                except (LinAlgWarning, LinAlgError):
                    #print('Ill-conditioned matrix, alpha solved using least_square')
                    self.alpha_[c, b, :] = np.linalg.lstsq(left_part[b, :, :],
                                                           right_part[b, :])[0]
            warnings.resetwarnings()
        # Compute log_likelihood_
        if compute_likelihood:
            self.log_marginal_likelihood_value_ = 0
            for c in range(self.n_classes_):
                for b in range(self.n_bands_):
                    self.log_marginal_likelihood_value_ += \
                        self.log_marginal_likelihood(None,
                                                     None,
                                                     X_train_[self.index_C[c], :],
                                                     t_[self.index_C[c]],
                                                     c, b,
                                                     False)
        return self

    def predict(self, X):
        """Return classes estimates for the test vector X, using MAP.
        Parameters
        ----------
        X:  array-like, shape = (n_samples, 1 + n_features, T_y instants)
            the first feature is the observation instants of X
        Returns
        -------
        C : array-like, shape = (n_samples)
            Returns the class for each sample in the model.
        """
        return self.classes_[np.argmax(self.predict_proba(X), axis=-1)]
 
    def predict_proba(self, X):
        """Return probability estimates for the test vector X.
        Parameters
        ----------
        X:  array-like, shape = (n_samples, 1 + n_features, T_y instants)
            the first feature is the observation instants of X
        Returns
        -------
        C : array-like, shape = (n_samples, n_classes)
            Returns the probability of the samples for each class in
            the model. The columns correspond to the classes in sorted
            order, as they appear in the attribute ``classes_``.
        """
        X_ = X[:, 1:]
        t = X[:, 0]
        n_samples = X_.shape[0]
        probas = np.zeros([n_samples, self.n_classes_])    # save probabilities

        # Init design matrix
        self.init_design_matrix(t)

        for i in range(n_samples):
            t_i = np.array(t[i]).reshape(-1, 1)

            # Loop on each class
            for c in range(self.n_classes_):
                log_like = 0
                for b in range(self.n_bands_):
                    # Compute design matrix (t_ny dependency)
                    Bi = self.design_matrix(t_i.squeeze(), in_memory=True)
                    # Center X
                    X_c = X_[i, b] - np.dot(Bi, self.alpha_[c, b])

                    L = self._chol_factor(t_i,
                                          self.kernels["{0}{1}".format(c, b)])

                    # Compute numerator
                    # log_pdf = self._chol_gauss_logpdf(X_c, L)
                    Q = cho_solve((L, True), X_c)
                    q = np.dot(X_c.T, Q)  # (x - mu).T*Sig^-1*(X - mu)

                    log_pdf = - 0.5 * q - np.sum(np.log(np.diag(L)))
                    # Bands are independent
                    log_like += log_pdf

                # log_likelihood + log_prior #{Zi = c} * P(y | Z = c)
                probas[i, c] = log_like + np.log(self.weights_[c])

            # scale for exp
            prior_max = probas[i, :].max()
            # LogSumExp trick for denominator
            LSE = prior_max + np.log(np.sum(np.exp(probas[i] - prior_max)))
            # compute posterior: #{Zi = c} * P(y | Z = c) / sum_i(P(y | Z = i))
            probas[i] = np.exp(probas[i] - LSE)
        # probas = softmax(probas, axis=1)

        return probas

    def score(self, X, y_true):
        """Return probability estimates for the test vector X.
        Parameters
        ----------
        X:      array-like, shape = (n_samples, 1 + n_features, T_y instants)
                the first feature is the observation instants of X
        y_true: array-like, shape = (n_samples, 1 class)
        Returns
        -------
        Score : float,
            Returns the model's accuracy.
        """
        return accuracy_score(y_true, self.predict(X))

    def input_missing_values(self, X, t, c = None, return_std = False, return_cov = False):
        """Return time-series values estimated for the test vector X, using MAP.
        Parameters
        ----------
        X:  array-like, shape = (n_samples, 1 + n_features, T_y instants)
            the first feature is the observation instants of X
        t: array_like, shape = (n(y) instants)
            contains the n(y) instants to input to signal X
        c:  int, default None
            class of the signal, if c = None: Applied Bayes decision rule
        return_std: bool, default False
            if True returns standard deviation of the signal to the mean.
        return_cov: bool, default False
            if True returns the covariance matrix associated to the process
        Returns
        -------
        mean_values: array-like, shape = (n(y), 1 + n_features)
            Returns the n(y) instants values for each feature.
        var: array-like float, shape = (n(y), n_features)
            if return_std, Variance evaluated on each point of the inputed time-series
        cov: Array-like, shape = (n_features, n(y), n(y))
            Covariance matrix associated to the inputed elements
        """
        if return_std and return_cov:
            raise RuntimeError("Not returning standard deviation of predictions when "
                               "returning full covariance.")
        t_ = check_array(t)
        
        x_ = X[1:]
        t_train = X[0].reshape([-1,1])
        

        if not hasattr(self, "kernels"): # Unfitted;no prediction
            raise RuntimeError("Model not fitted.")
        else:
            # Create predictive mean/var values and covariance matrix,
            #    mean values is containing times and values for each band
            mean_values = np.zeros([t_.shape[0], self.n_bands_ + 1])
            if return_cov:
                cov_mat = np.zeros([self.n_bands_, t_.shape[0], t_.shape[0]])
            elif return_std:
                var_values = np.zeros([t_.shape[0], self.n_bands_])


            # Compute mean (and var, cov)
            if (c == None):
                # Prediction when class is unknown
                probas_c = self.predict_proba(np.array([X]))
                probas_c[0, probas_c[0,:] < 1e-20] = 0    # avoid approximation on zero machine

                if return_cov:
                    # Return mean and covariance matrix
                    expectations_squared_c = np.zeros([self.n_bands_, t_.shape[0], t_.shape[0]])

                    for ci in range(self.n_classes_):
                        # Get values when c is known
                        expectations_cov_c = self.input_missing_values(X, t_,
                                                                       c = self.classes_[ci],
                                                                       return_cov=True)
                        # Save and ponderate them
                        mean_values += expectations_cov_c[0] * probas_c[0,ci]
                        # save covariance matrix for each class, weighted
                        cov_mat += expectations_cov_c[1] * probas_c[0,ci]

                        # compute correction term
                        for b in range(self.n_bands_):
                            expectations_squared_c[b]+= \
                                                np.dot(expectations_cov_c[0][:,(1+b):(2+b)],
                                                       expectations_cov_c[0][:,(1+b):(2+b)].T) \
                                                       * probas_c[0,ci]
                    
                    # correction terms in covariance
                    cov_mat += expectations_squared_c - \
                            np.matmul(mean_values[:,1:].reshape([self.n_bands_,t_.shape[0],1]),
                                      mean_values[:,1:].reshape([self.n_bands_,1,t_.shape[0]]))
                    
                    cov_mat_negative = cov_mat < 0
                    if np.any(cov_mat_negative):
                        warnings.warn("Corrected covariance matrix smaller than 0. "
                                      "Setting those values to 0.")
                        cov_mat[cov_mat_negative] = 0.0
                    
                elif return_std:
                    # Save expectations and std deviations for each classe, weighted by
                    #   its probability to belong to class c
                    expectations_squared_c = np.zeros([t_.shape[0],self.n_bands_])
                    
                    for ci in range(self.n_classes_):
                        # Get values when c is known
                        expectations_std_c = self.input_missing_values(X, t_,
                                                                       c = self.classes_[ci],
                                                                       return_std=True)
                        # Save and ponderate them
                        mean_values += expectations_std_c[0] * probas_c[0,ci]
                        expectations_squared_c += expectations_std_c[0][:,1:] \
                                                  * expectations_std_c[0][:,1:] * probas_c[0,ci]
                        # we use square since square root is the output
                        var_values += expectations_std_c[1]*expectations_std_c[1]*probas_c[0,ci]

                    # correction terms in variance
                    var_values += expectations_squared_c - mean_values[:,1:]*mean_values[:,1:]
                    
                    x_var_negative = var_values < 0
                    if np.any(x_var_negative):
                        warnings.warn("Corrected variances smaller than 0. "
                                      "Setting those variances to 0.")
                        var_values[x_var_negative] = 0.0

                # Case when return_cov and return_std are False
                else:
                    expectations_c = np.array([self.input_missing_values(X, t_,
                                                                         c = self.classes_[ci]) \
                                               *probas_c[0,ci]
                                               for ci in range(self.n_classes_)])
                    mean_values = expectations_c.sum(axis = 0)

            else:
                # Prediction when class is known
                mean_values[:, 0] = t_[:,0]               # Save temporal instants
                c_ = np.where(self.classes_ == c)[0][0]      # Uses labelEncoder defined in fit

                
                for b in range(self.n_bands_):
                    K = clone(self.kernels["{0}{1}".format(c_, b)])
                    L = self._chol_factor(t_train, K)
                        
                    # Data - multi-dimentional centered output, y in formula
                    Bi = self.design_matrix(t_train)
                    mean = np.dot(Bi, self.alpha_[c_,b])
                    # Center X_train_
                    Xc = x_[b] - mean
                    if Xc.ndim == 1:
                        Xc = Xc[:, np.newaxis]
                        
                    K_inv_prod_Xc = cho_solve((L, True), Xc)
                    K_trans = K(t_, t_train)
                    
                    # Compute mean (right part)
                    x_mean = K_trans.dot(K_inv_prod_Xc)      # Line 4
                    # Compute mean on target points (B*alpha)
                    B = self.design_matrix(t_, b)
                    x_mean = np.dot(B, self.alpha_[c_, b]) + x_mean # undo normal
                    # update values for the band b
                    mean_values[:, b+1] = np.diag(x_mean)
                    
                    if return_cov:
                        # Compute K(t*) - K_trans (K^-1) K_trans.T
                        v = cho_solve((L, True), K_trans.T) # Line 5
                        cov_mat[b] = K(t_) - K_trans.dot(v)  # Line 6

                    elif return_std:
                        # compute inverse K_inv of K based on its Cholesky
                        # decomposition L and its inverse L_inv
                        L_inv = solve_triangular(L.T,
                                                 np.eye(L.shape[0]))
                        self._K_inv = L_inv.dot(L_inv.T)

                        # Compute variance of predictive distribution
                        var_values[:,b] = K.diag(t_)
                        var_values[:,b] -= np.einsum("ij,ij->i",
                                                     np.dot(K_trans, self._K_inv), K_trans)

                        # Check if any of the variances is negative because of
                        # numerical issues. If yes: set the variance to 0.
                        x_var_negative = var_values[:,b] < 0
                        if np.any(x_var_negative):
                            warnings.warn("Predicted variances smaller than 0. "
                                          "Setting those variances to 0.")
                            var_values[x_var_negative, b] = 0.0
                        
                      
        if return_cov:
            return mean_values, cov_mat
        if return_std:
            return mean_values, np.sqrt(var_values)
        return mean_values

    def log_marginal_likelihood(self, theta=None, eval_gradient=False,
                                X_train_C=None, t_C=None, c=None, b=None,
                                compute_alpha=True):
        """Compute the LML, and the Gradient for a given class and band
        Parameters
        ----------
        theta      , list of theta (c and b)
        eval_gradient, evaluation of the gradient
        args    [0], X_train_C, array training samples for class c
                [1], t_C      , array of lists, observation instants
                [2], c, class considered
                [3], b, band to be optimized
        Returns
        -------
        LML, grad LML
        """
        nc = X_train_C.shape[0]

        # Check params
        if (theta is None) and (eval_gradient is True):
            raise ValueError(
                "Gradient can only be evaluated for theta!=None")
            return self.log_marginal_likelihood_value_

        # Initialization
        if eval_gradient:
            log_likelihood_gradient_ = np.zeros([theta.size])
        log_likelihood_ = 0

        # Set Kernels
        if theta is None:
            kernel_ = self.kernels["{0}{1}".format(c, b)]
        else:
            kernel_ = self.kernels["{0}{1}".format(c, b)].clone_with_theta(theta)

        # First step: Compute G, D and H
        G = np.zeros((self.n_basis_, self.n_basis_))
        D = np.zeros((self.n_basis_))

        for i in range(nc):
            # Compute design matrix
            Bi = self.design_matrix(t_C[i], in_memory=True)

            # Compute kernel matrix
            K = kernel_(t_C[i].reshape([t_C[i].shape[0], 1]))
            K[np.diag_indices_from(K)] += self.epsilon

            # Solve linear problem
            L = cho_factor(K, lower=True)
            M = cho_solve(L, Bi).T
            
            # Update leaft and right parts
            G += np.dot(M, Bi)
            D += np.dot(M, X_train_C[i, b])
            
        # Avoid to print warning from scipy.linalg
        warnings.filterwarnings("error", category = LinAlgWarning)
        try:
            alpha = solve(G, D)
        except (LinAlgWarning, LinAlgError):
            #print('Ill-conditioned matrix, alpha solved using least_square')
            alpha = np.linalg.lstsq(G, D)[0]
        warnings.resetwarnings()

        # Compute H for gradient
        if eval_gradient and self.opt_gradient == 'complete':
            H = np.zeros((theta.size, self.n_basis_))
            for i in range(nc):
                K, Sigma_gradient = kernel_(t_C[i].reshape([t_C[i].shape[0], 1]),
                                                eval_gradient=True)
                # Compute design matrix
                Bi = self.design_matrix(t_C[i], in_memory=True)

                # Compute kernel matrix
                K[np.diag_indices_from(K)] += self.epsilon

                # Solve linear problem
                L = cho_factor(K, lower=True)
                M = cho_solve(L, Bi).T
                Sigma_inv_dot_xc = cho_solve(L, X_train_C[i, b] - np.dot(Bi, alpha))

                for theta_ in range(theta.size):
                    M_grad = np.dot(M, Sigma_gradient[:, :, theta_])
                    H[theta_] += np.dot(M_grad, Sigma_inv_dot_xc)

            alpha_corrected = np.empty((theta.size, self.n_basis_))
            # Compute alpha_corrected
            for theta_ in range(theta.size):
                warnings.filterwarnings("error", category = LinAlgWarning)
                try:
                    alpha_corrected[theta_] = solve(G,
                                                    H[theta_])
                except (LinAlgWarning, LinAlgError):
                    alpha_corrected[theta_] = np.linalg.lstsq(G,
                                                              H[theta_])[0]
                warnings.resetwarnings()

        # Second step: Compute LML and gradient
        for i in range(nc):
            # Compute design matrix
            Bi = self.design_matrix(t_C[i], in_memory=True)

            Xc = (X_train_C[i, b] - np.dot(Bi, alpha))
            if Xc.ndim == 1:
                Xc = Xc[:, np.newaxis]

            # Compute kernel matrix
            if eval_gradient:
                Sigma, Sigma_gradient = kernel_(t_C[i].reshape([t_C[i].shape[0], 1]),
                                                eval_gradient=True)
            else:
                Sigma = kernel_(t_C[i].reshape([t_C[i].shape[0], 1]))
            Sigma[np.diag_indices_from(Sigma)] += self.epsilon
            
            # Solve linear problem
            L = cho_factor(Sigma, lower=True)
            Sigma_inv_dot_xc = cho_solve(L, Xc)

            # Likelyhood
            log_likelihood_ -= (2*np.log(np.diag(L[0])).sum()
                                + np.dot(Xc.T, Sigma_inv_dot_xc)
                                + Sigma.shape[0] * np.log(2*np.pi))  # def constant value

            if eval_gradient:  # This part could be computed faster wt trace
                Sigma_inv = cho_solve(L, np.eye(L[0].shape[0]))
                for theta_ in range(theta.size):
                    log_likelihood_gradient_[theta_] -= np.dot(Sigma_inv,
                                                               Sigma_gradient[:, :, theta_]).trace()
                    log_likelihood_gradient_[theta_] += np.dot(
                        np.dot(Sigma_inv_dot_xc.T,
                               Sigma_gradient[:, :, theta_]),
                        Sigma_inv_dot_xc)
                    if self.opt_gradient == 'complete':
                        log_likelihood_gradient_[theta_] -= 2*np.dot(
                            Sigma_inv_dot_xc.T,
                            np.dot(Bi, alpha_corrected[theta_]))

        # Return LML
        if eval_gradient:
            return log_likelihood_, log_likelihood_gradient_
        else:
            return log_likelihood_

    def init_design_matrix(self, t_):
        """ Initialization of the design matrix
        Compute only once the whole matrix then for each call copy only
        the corresponding part.
        """
        # Compute position of each temporal
        T = np.unique(np.hstack(t_))
        if self.tau is None:
            self.tau = int(2 * T.max()) + 1      # Save maximum to define tau
        self.dict_position = {ti: i for i, ti in enumerate(T)}

        # Precompute the whole basis
        J = np.linspace(0, self.n_basis_-1,
                        self.n_basis_, dtype=int)
        self.B = np.empty([T.size, self.n_basis_])

        for i, t in enumerate(T):
            # func_base should return all the basis for a given t
            self.B[i, :] = self.func_base(t, J, self.tau)

    def design_matrix(self, t, in_memory=False):
        """
        Matrix B in the paper.
        Conditions on band must be written here to design the model.

        Input : t, observation instants
        Output: B(t)
        """
        if in_memory is True:
            positions = [self.dict_position[t_] for t_ in t]
            B = self.B[positions, :]
        else:
            len_t = len(t)

            B = np.zeros([len_t, self.n_basis_])
            j = np.linspace(0, self.n_basis_-1, self.n_basis_, dtype=int)
            for l in range(len_t):
                B[l] = self.func_base(t[l], j, self.tau)

        return B

    def _constrained_optimization(self, obj_func, initial_theta, bounds,
                                  X, t, c, b):
        if self.optimizer == "fmin_l_bfgs_b":
            theta_opt, func_min, convergence_dict = \
                                fmin_l_bfgs_b(obj_func, initial_theta,
                                              args=(True, X, t, c, b),
                                              bounds=bounds,
                                              maxls=20)
            if convergence_dict["warnflag"] != 0:
                warnings.warn("fmin_l_bfgs_b terminated abnormally with the "
                              " state: %s" % convergence_dict,
                              ConvergenceWarning)
        elif callable(self.optimizer):
            theta_opt, func_min = self.optimizer(obj_func, initial_theta,
                                                 args=(True, X, t, c, b),
                                                 bounds=bounds)
        else:
            raise ValueError("Unknown optimizer %s." % self.optimizer)

        return theta_opt, func_min

    # Maths function
    def _chol_factor(self, x, K, eval_gradient = False):
        """ Compute Cholesky factor on a gaussian_process kernel,
            on a vector x sized (Ti samples, 1),
            including an gradient evaluation on the kernel.
        Parameters
        ----------
        x:  (Ti, 1),  points where the kernel is evaluated
        K:  (Ti, Ti), kernel (definite-positive matrix)
        eval_gradient: default False, evaluate gradient on kernel K
        Returns
        -------
        L:          (Ti, Ti), Cholesky factor.
        K_gradient: (Ti, Ti), if eval_gradient == True only,
                      Gradient evaluated on kernel K.
        """
        # Cholesky decomposition [RW2006] Alg 2.1, p. 19; Alg 5.1, p. 126
        d = x.shape[0]
        
        kernel = clone(K)
        if eval_gradient:
            Ki, K_gradient = kernel(x.reshape([d,1]), eval_gradient = True)
        else:
            Ki = kernel(x.reshape([d, 1]))

        Ki[np.diag_indices_from(Ki)] += self.epsilon
        L = cholesky(Ki, lower=True)  # Line 2

        if eval_gradient:
            return L, K_gradient
        return L
    
    def _chol_gauss_logpdf(self, Xc, L):
        """log-pdf for gaussian distributuion with full covariance matrix
           (cholesky factorization for stability)
        Parameters
        ----------
        Xc shape : D, N
        L shape (cholesky of cov matrix) : D,D
        Returns
        -------
        pdf shaped: N, (2pi) ** k is removed since we normalize after
        """
        Xn = np.array(Xc)
        Q = cho_solve((L, True), Xn); q = np.dot(Xn.T, Q)  # (x - mu).T*Sig^-1*(X - mu)
        
        log_pdf = - 0.5 * q - np.sum( np.log(np.diag(L)) )
    
        return log_pdf

