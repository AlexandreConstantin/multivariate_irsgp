# coding: utf-8

import sys


from simulations_papier import *

# Read variables
seed = int(sys.argv[1])     # random seed
T_i_ = int(sys.argv[2])     # Longueur des series temporelles
beta_ = float(sys.argv[3])  # Dépendance spectrale (0 faible -> 1 forte)


# Variables
n_ech = 20 # Nombre series temporelles
tau_ = 1000  # Taille de la grille d'echantillonage dans [0, 1]
n_c = 2      # Nombre de classes
n_b = 10     # Nombre de bandes

lambda_ = 0.01  # Variability of mean coefficients


####
# Train / Test split
####
ct = int(n_ech/2)

# extract functions
for c in range(n_c):
    # Fixed seed for alpha
    np.random.seed(c*2)
    # Extract mean and covariances matrices
    A_c_, means_c, alpha_c = generate_mean(c = c, p = n_b, beta = beta_,
                                           lambda_ = lambda_, tau = tau_)
    
    # Set seed
    np.random.seed(seed)
    # Generate random multivariate normal signals
    Gps_, complete_GPs_, Cs_ = generate_GPs(c = c, A_c = A_c_, means_c = means_c,
                                            n_times = T_i_, level = level_,
                                            n_sig = n_ech, tau = tau_)
    # Concatenate for each class
    if c == 0:
        # Train
        Gps_train, Cs_train = Gps_[:ct, ...], Cs_[:ct]
        complete_GPs_train = complete_GPs_[:ct, ...]
        # Test
        Gps_test, Cs_test = Gps_[ct:, ...], Cs_[ct:]
        complete_GPs_test = complete_GPs_[ct:, ...]
        
        # Correlation
        As_c = A_c_[np.newaxis, ...]
        alphas_c = alpha_c[np.newaxis, ...]
    else:
        # Train
        Gps_train = np.concatenate([Gps_train, Gps_[:ct, ...]], axis = 0)
        complete_GPs_train = np.concatenate([complete_GPs_train, complete_GPs_[:ct, ...]], axis = 0)
        Cs_train = np.concatenate([Cs_train, Cs_[:ct]], axis = 0)
        # Test
        Gps_test = np.concatenate([Gps_test, Gps_[ct:, ...]], axis = 0)
        complete_GPs_test = np.concatenate([complete_GPs_test, complete_GPs_[ct:, ...]], axis = 0)
        Cs_test = np.concatenate([Cs_test, Cs_[ct:]], axis = 0)
        
        # Correlation
        As_c = np.concatenate([As_c, A_c_[np.newaxis, ...]], axis = 0)
        alphas_c = np.concatenate([alphas_c, alpha_c[np.newaxis, ...]], axis = 0)
        
CLASSES = np.unique(Cs_train)
J = alphas_c.shape[2]

####
# Model
####
# Paramètres du modèle
K_dep = Ck(1, constant_value_bounds="fixed") * RBF(length_scale=1, length_scale_bounds=(1e-5, 1e5)) \
    + WhiteKernel(0.01, noise_level_bounds="fixed")
K_indep = Ck(1) * RBF(length_scale=1, length_scale_bounds=(1e-5, 1e5)) \
    + WhiteKernel(0.001)

basis_ = "fourier"
n_basis_ = int((J - 1)/2)

eps_ = 1e-12

# Models
model = M2GP(kernel = K_dep, epsilon = eps_,
             base = basis_, n_basis = n_basis_,
             tau = tau_)

model_indep = MIMGP(kernel = K_indep, epsilon = eps_,
                    base = basis_, n_basis_ = n_basis_,
                    tau = tau_)
# Fit (or load)
model.fit(Gps_train, Cs_train)
model_indep.fit(Gps_train, Cs_train)



###
# Scores
###

# Predict classes
results = np.empty((3, Gps_test.shape[0]))
results[0, :] = model_indep.predict(Gps_test)
results[1, :] = model.predict(Gps_test)
results[2, :] = Cs_test

# Reconstruct time-series
t_input = np.arange(0, 1000, 10)
t_ = t_input.reshape(-1, 1)

# Imputation error, first dimension is for the model: 0-indep / 1-dep
reconstr_tab = np.empty((3, Gps_test.shape[0], n_b, CLASSES.shape[0], t_input.shape[0]))
for i in range(Gps_test.shape[0]):
    r_ = model.input_missing_values(Gps_test[i, :], t_, c = None)
    r_indep = model_indep.input_missing_values(Gps_test[i, :], t_, c = None)
    
    # Save error
    for b in range(n_b):
        reconstr_tab[0, i, b, Cs_test[i], :] = r_indep[:, b+1]
        reconstr_tab[1, i, b, Cs_test[i], :] = r_[b+1, :]
        reconstr_tab[2, i, b, Cs_test[i], :] = complete_GPs_test[i, b+1, t_input]

###
# Parameters
###
thetas = np.empty((len(CLASSES), len(model.kernels["0"].theta)))
thetas_indep = np.empty((len(CLASSES), n_b, len(model_indep.kernels["00"].theta)))

for c in CLASSES:
    thetas[c, :] = model.kernels["{}".format(c)].theta
    
    for b in range(n_b):
        thetas_indep[c, b] = model_indep.kernels["{}{}".format(c,b)].theta

alphas_c = np.concatenate([alphas_c, model.alpha_], axis = 0)
alphas_c = np.concatenate([alphas_c, model_indep.alpha_], axis = 0)

As_c = np.concatenate([As_c, model.AA_T_], axis = 0)

    
###
# Save
###
np.savez("./results_simus/results_{}_{}_{}.npz".format(
            seed, T_i_, int(beta_*100)),
         classes = results,
         reconstr = reconstr_tab,
         thetas = thetas,
         thetas_indep = thetas_indep,
         alphas = alphas_c,
         AcAcT = As_c)

