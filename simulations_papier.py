# coding: utf-8

import numpy as np


# Scikit kernels
from sklearn.gaussian_process.kernels import RBF, ConstantKernel as Ck, WhiteKernel

# Model
from mult_model import M2GP
from indep_model import MIMGP


# Parameters
n_sig_per_class = 100
n_classes = 2
nFeatures = 10
n_times_ = 10

level_ = 5
tau_ = 1000


def fourier_basis_functions(t, J, tau):
    """
    Input : t,   float, instant of observation
            J,     int, number of basis function
            tau, float, observed period
    Output: array[float], fourier basis function
    """
    n_bases = J.size
    n_functions = int((J.size-1)/2)
    b = np.empty((n_bases,))
    b[0] = 1
    for j in range(1, 1+n_functions):
        b[j] = np.cos(2*np.pi*j*t/tau)
        b[n_functions+j] = np.sin(2*np.pi*j*t/tau)

    return b

def generate_mean(c = 0, p = 3, beta = 0, lambda_ = 0.1, tau = 1):
    # Times
    t_ = np.arange(0, tau+1)
    # Basis function
    J = 5*2+1
    Js = np.linspace(0, J-1, J, dtype=int)
    B = np.empty((J, t_.size))
    for l, t in enumerate(t_):
        B[:, l] = fourier_basis_functions(t, Js, tau)
    alpha_c = np.empty((p, J))
    
    if c == 0:
        alpha_c[:, 0] = np.random.normal(0, lambda_, size = p)
    else:
        alpha_c[:, 0] = np.random.normal(0, lambda_, size = p)

    for b in range(p):
        alpha_c[b, 1:] = np.random.normal(0, lambda_, size = J-1)
    
    means = np.dot(alpha_c, B)

    # Wavelengths
    AA_T_c = np.full((p, p), fill_value = beta)
    AA_T_c[np.diag_indices_from(AA_T_c)] = 1
    # Compute A_c from AA_T
    if beta < 1:
        A_c = np.linalg.cholesky(AA_T_c)
    else:
        A_c = np.full((p, p), fill_value = 1/np.sqrt(p))
        
    return A_c, means, alpha_c

def generate_GPs(c, A_c, means_c, n_times = 10, level = 1, n_sig = 100, tau = 365):
    # Init
    complete_GPs = np.empty((n_sig, A_c.shape[0] + 1, tau+1))
    GPs = [[] for _ in range(n_sig)]
    Cs = []
    # Lengths scales
    l_c = [120, 250]
    l_b_step = [0, 0]
    # Number of times
    all_times = np.arange(tau+1)
    null_mean = np.zeros_like(all_times)

    # Draw random variate Gaussian
    for b in range(A_c.shape[0]):
        Kbc = level * (RBF(length_scale = l_c[c] + l_b_step[c]*b,
                           length_scale_bounds=(1e-5, 1e5)) \
                       + WhiteKernel(0.5))
        Kt = Kbc(all_times.reshape([-1, 1]))
        complete_GPs[:, b+1, :] = np.random.multivariate_normal(null_mean, Kt,
                                                                size = n_sig)
    # Extract for each time series
    for i in range(n_sig):
        # Random times
        if i == 0:
            # First we add one more time to get the shape (n_sig, n_bands + 1) containing arrays of size T_i
            #   otherwise it is concatenated into a single array (n_sig, n_bands + 1, n_sig)
            t_i = np.sort(np.random.choice(all_times, n_times+1, replace = False))
        else:
            t_i = np.sort(np.random.choice(all_times, n_times, replace = False))
        # Fill each samples
        complete_GPs[i, 0, :] = all_times
        GPs[i].append(t_i)

        # Transform GPs
        complete_GPs[i][1:] = np.dot(A_c, complete_GPs[i][1:])
        complete_GPs[i][1:] += means_c[:, all_times]
        for b in range(A_c.shape[0]):
            GPs[i].append(complete_GPs[i, b+1, t_i])

        # Fill classes
        Cs.append(c)

    return np.array(GPs, dtype = object), np.array(complete_GPs, dtype = object), np.array(Cs)


def main(seed = 0, n_c = 3, p = 3, n_times = 10, level = 1, beta = 0, tau = 365, n_sigc = 100):
    
    np.random.seed(seed)

    for c in range(n_c):
        # Extract mean and covariances matrices
        A_c_, means_c, alpha_c = generate_mean(c = c, p = p, beta = beta, tau = tau)
        # Generate random multivariate normal signals
        Gps_, complete_GPs_, Cs_ = generate_GPs(c = c, A_c = A_c_, means_c = means_c,
                                                n_times = n_times, level = level,
                                                n_sig = n_sigc, tau = tau)
        # Concatenate for each class
        if c == 0:
            Gps, Cs = Gps_, Cs_
            A_c = A_c_[np.newaxis, ...]
        else:
            Gps = np.concatenate([Gps, Gps_], axis = 0)
            Cs = np.concatenate([Cs, Cs_], axis = 0)
            A_c = np.concatenate([A_c, A_c_[np.newaxis, ...]], axis = 0)

    CLASSES = np.unique(Cs)
    # Learn model
    K = RBF(length_scale=1, length_scale_bounds=(1e-5, 1e5)) \
                          + WhiteKernel(10)
    basis_ = "fourier"
    n_basis_ = 5
    model = M2GP(kernel = K, epsilon = 1e-12, base = basis_, n_basis = n_basis_, tau = tau_)
    model_indep = MIMGP(kernel = K, epsilon = 1e-12, base = basis_, n_basis_ = n_basis_, tau = tau_)

    model.fit(Gps, Cs)
    model_indep.fit(Gps, Cs)
    
    # Print parameters
    for key in model.kernels:
        print("Kernel {}: {}".format(CLASSES[int(key)], model.kernels[key]))
    for key in model_indep.kernels:
        print("Kernel {}: {}".format(key, model_indep.kernels[key]))
    for c in range(CLASSES.shape[0]):
    #     print(c)
        AA_cT_true = np.dot(A_c[c], A_c[c].T)
    #     ck = np.mean(AA_cT_true) / np.mean(model.AA_T_[c])
    #     print(ck)
    #     print(model.AA_T_[c])
        print(AA_cT_true)
    
    return 0

if __name__ == "__main__":
    beta_ = 0.1
    seed_ = 0
    main(seed = seed_, n_c = n_classes, p = nFeatures,
         beta = beta_, level = level_,
         n_times = n_times_, tau = tau_,
         n_sigc = n_sig_per_class)
